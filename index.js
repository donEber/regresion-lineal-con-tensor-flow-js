// Creamos un Tensor (Un tensor es un arreglo o matriz, depende si lo especificamos)
/* tensorY */
let valoresY = [1, 0, 0, 1]; // 'valoresY' seran los valores del tensorY
// El tensor (que creamos con 'tf') puede recibir hasta 3 paramentros
// (valores , dimencion , tipoDato)
const tensorY = tf.tensor2d(valoresY, [2, 2], 'int32');
tensorY.print()
/* tensorX*/
//Asignamos al tensor numeros aleatorios
let valoresX = [];
for (let i = 0; i < 4; i++) {
    valoresX.push(Math.random(0, 100) * 10);
}
const tensorX = tf.tensor2d(valoresX, [2, 2], 'int32');
tensorX.print()
//Multiplicamos e imprimimos los tensores X e Y
tf.matMul(tensorX, tensorY).print()